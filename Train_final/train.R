#Author:    Robbin Bouwmeester (supervisors: Erik van Dijk & Sanne Abeln)
#Purpose:   Training the final TFM model and printing the model so it can be implemented in a different language
#Date:      19-06-2017
#R-version: 3.10
#Bugs:      No bugs found...

require(caret)
require(Cubist)

tmID <- read.delim("PDBTM.txt", header = FALSE, sep = "\t", quote = "\"", dec = ".",
                   comment.char = "#") #fill = TRUE
tmID <- tmID[,1]
tmID <-  c(tmID,0.0,"0.0")

train <- read.delim("train.txt", header = TRUE, sep = "\t", quote = "\"", dec = ".",
                         fill = TRUE, comment.char = "#")

train <- train[!(train[,1] %in% tmID),]

ctrl <- cubistControl(unbiased = FALSE, 
                      rules = 50, 
                      extrapolation = 1,
                      sample = 0, 
                      seed = 42,
                      label = "outcome")

model <- cubist(train[,c("length","Hydrophobic","Hydrophylic")],train[,c("HydrophobicA")],committees=1,control = ctrl)

summary(model)