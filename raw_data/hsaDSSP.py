import gzip
import sys
from optparse import OptionParser
import os

def parse_commandline():
    usage = "%prog <PDB> [options]"
    version = "1.0a"
    description = \
        "Predicting the Hydrophobic Surface Area and getting frequencies from fasta"
    epilog = \
        "Option Parser Copyright (c) 2015 Robbin Bouwmeester"
    parser = OptionParser(usage=usage, description=description,
                          version="%prog "+version, epilog=epilog)

    #Parse the commandline options
    parser.add_option("-i", "--input",  dest="path", metavar="<file>",
                     help="input dssp file")
    parser.set_defaults(path="")
    
    #Parse the commandline options
    parser.add_option("-d", "--dir",  dest="dir", metavar="<file>",
                     help="input directory with protein dssp files")
    parser.set_defaults(dir="")
    
    #Parse the commandline options
    parser.add_option("-o", "--output",  dest="outfile", metavar="<file>",
                     help="specify an output file, default is out.txt")
    parser.set_defaults(outfile="out.txt")
    
    #Get the options:
    (options, args) = parser.parse_args()
    
    #if len(options.path) == 0:
    #    sys.stderr.write("Fasta file not specified, will crash. Use the -i parameter to specify a file.\n")
    #    sys.exit(-1)
        
    #Check for any leftover command line arguments:
    if len(args):
        sys.stderr.write("Ignoring additional arguments %s.\n" % (str(args)))
    
    #Clean up
    del(parser)
    return options 

def parseDSSP(infile,hydrophobic=["A","F","C","L","I","W","V","M","Y"]):
    hsa = 0.0
    startReading = False
    for line in infile:
        if line.startswith("#"): continue
        if line.startswith("  #  RESIDUE AA STRUCTURE"): 
            startReading = True
            continue
        if not(startReading): continue
        #line = line.strip()
        if len(line) == 0: continue
        
        aa = line[13]
        #print line.split()
        if aa in hydrophobic: hsa += float(line[34:38].strip())
    return hsa

def writeToFile(id,hsa,outfile,write_type="a"):
    outfile_handle = open(outfile,write_type)
    outfile_handle.write("%s\t%s\n" % (id,str(hsa)))
    outfile_handle.close()

def main():
    options = parse_commandline()
    if options.dir != "":
        files = [os.path.join(options.dir, file) for file in os.listdir(options.dir)]
    else:
        files = [options.path]
    writeToFile("id","HSA",options.outfile,write_type="w")
    for file in files:
        if file.endswith(".gz"): infile = gzip.open(file)
        else: infile = open(file)
        
        id = os.path.basename(file).split(".")[0]
        if id.startswith("pdb"): id = id[3:]
        if id.endswith("ent"): id = id[:-3]
        
        writeToFile(id,parseDSSP(infile),options.outfile)
        
main()