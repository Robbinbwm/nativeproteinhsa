import gzip
import sys
from optparse import OptionParser
import os

def getTFMFreq(infile,hydrophobics=["A","V","I","L","M","F","Y","W"],hydrophylics = ["K","R","H","N","S","T","E","N","Q"]):
    length = 0
    nHydrophobics = 0
    nHydrophylics = 0
    
    for line in infile:
        if line.startswith(">"): continue
        line = line.strip().upper()
        for aa in line:
            if aa in hydrophobics: nHydrophobics += 1
            if aa in hydrophylics: nHydrophylics += 1
            length += 1

    return (nHydrophobics,nHydrophylics,length)

def getAAFreq(infile,amino_acids=["A","V","I","L","M","F","Y","W","K","R","H","N","S","T","E","N","Q","C","G","P"],return_aa_order=False):
    import collections
    
    seq = ""
    for line in infile:
        if line.startswith(">"): continue
        seq += line.strip().upper()
    counter = collections.Counter(seq)
    
    freq_vector = []
    for aa in amino_acids:
        try:
            freq_vector.append(counter[aa])
        except KeyError:
            freq_vector.append(0)

    if return_aa_order:
        return (freq_vector,amino_acids)
    else:
        return freq_vector


"""Function that creates an option parser
        Inputs:
            -
        Outputs:
            options - dictionary that contains the options parsed that were supplied by the user
"""
def parse_commandline():
    usage = "%prog <PDB> [options]"
    version = "1.0a"
    description = \
        "Predicting the Hydrophobic Surface Area and getting frequencies from fasta"
    epilog = \
        "Option Parser Copyright (c) 2015 Robbin Bouwmeester"
    parser = OptionParser(usage=usage, description=description,
                          version="%prog "+version, epilog=epilog)

    #Parse the commandline options
    parser.add_option("-i", "--input",  dest="path", metavar="<file>",
                     help="input protein fasta")
    parser.set_defaults(path="")
    
    #Parse the commandline options
    parser.add_option("-d", "--dir",  dest="dir", metavar="<file>",
                     help="input directory with protein fasta")
    parser.set_defaults(dir="")
    
    #Parse the commandline options
    parser.add_option("-o", "--output",  dest="outfile", metavar="<file>",
                     help="specify an output file, default is out.txt")
    parser.set_defaults(outfile="out.txt")
    
    parser.add_option("-f", "",  dest="freqVect",  action="store_true",
                     help="Get a frequency vector for amino acids, add this flag.")
    parser.set_defaults(freqVect=False)
    
    parser.add_option("-t", "",  dest="freqVectTFM",  action="store_true",
                     help="Get a frequency vector for amino acids in TFM format, add this flag.")
    parser.set_defaults(freqVectTFM=False)
    
    #Get the options:
    (options, args) = parser.parse_args()
    
    #if len(options.path) == 0:
    #    sys.stderr.write("Fasta file not specified, will crash. Use the -i parameter to specify a file.\n")
    #    sys.exit(-1)
        
    #Check for any leftover command line arguments:
    if len(args):
        sys.stderr.write("Ignoring additional arguments %s.\n" % (str(args)))
    
    #Clean up
    del(parser)
    return options 

def writeToFile(id,vector,outfile,write_type="a"):
    outfile_handle = open(outfile,write_type)
    outfile_handle.write("%s\t%s\n" % (id,"\t".join(map(str,vector))))
    outfile_handle.close()
    
def main():
    options = parse_commandline()
    if options.dir != "":
        files = [os.path.join(options.dir, file) for file in os.listdir(options.dir)]
    else:
        files = [options.path]
    
    if options.freqVect: writeToFile("id",["A","V","I","L","M","F","Y","W","K","R","H","N","S","T","E","N","Q","C","G","P"],options.outfile,write_type="w")
    if options.freqVectTFM: writeToFile("id",["hydrophobics","hydrophylics","length"],options.outfile,write_type="w")
    
    for file in files:
        if file.endswith(".gz"): infile = gzip.open(file)
        else: infile = open(file)
        
        id = os.path.basename(file).split(".")[0]
        if id.startswith("pdb"): id = id[3:]
        
        if options.freqVect: writeToFile(id,getAAFreq(infile),options.outfile)
        if options.freqVectTFM: writeToFile(id,getTFMFreq(infile),options.outfile)

main()