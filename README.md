# README #

Either read the documentation supplied in the scripts or for the python scripts call:

```
#!shell

python [script].py --help
```

# FOLDERS #

The script TFM2.1.R can be found in the root folder and is used to create and compare the models. Please see the script itself for the documentation.

## raw_data/ ##

Contains the DSSP, fasta, PDB and individual surface accessibility predictions for the train, test and validation sets:

```
#!shell

raw_data/AllData.zip
```

Python script used to get frequency vectors (amino acid and TFM features) from fasta files:

```
#!shell

raw_data/getFreqVect.py
```

Python script used to get the target variable (hydrophobic surface area) from a PDB file:


```
#!shell

raw_data/hsaDSSP.py
```

Shell script used to rund DSSP on PDB files:

```
#!shell

raw_data/massDSSP.sh
```

Python script to get the HSA from individual surface accessibility predictions:

```
#!shell

raw_data/parseASA.py
```

## Datasets/ ##

Folder containing multiple datasets used to fit and compare the models. See TFM2.1.R for the description of the files.

## Datasets_supplements/ ##

Folder containing multiple datasets used to fit and compare the models used in the supplements. See TFM2.1.R for the description of the files.

## img/ ##

Output folder for images when TFM2.1.R is used.

## Train_final/ ##

Folder containing a R-script and datasets to train the final TFM model.

# CONTACT #

Robbin.bouwmeester@ugent.be

S.abeln@vu.nl